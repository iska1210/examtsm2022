# Python exam for TSM 
Run the following command before into Python Console starting
```
pip install -r requirements.txt
```
We used pip freeze to set up the core packages and updated it according to the packages we use.

# Config
You need to run the index.py file in order to see the whole flow of the application.
All the graph and answers will appear on the screen and in the console


# Sources
## For Part 1
Basic stackoverflow links for creation of class.

## For Part 2 
https://realpython.com/k-means-clustering-python/
https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
https://stackoverflow.com/questions/63697847/changing-label-names-of-kmean-clusters#:~:text=There%20isn't%20any%20way,to%20change%20the%20default%20labels.
https://stackabuse.com/seaborn-scatter-plot-tutorial-and-examples/
https://stackoverflow.com/questions/52285104/3d-scatterplots-in-python-with-hue-colormap-and-legend

## For Part 3
https://stats.stackexchange.com/questions/2641/what-is-the-difference-between-likelihood-and-probability
https://stackoverflow.com/questions/37487830/how-to-find-probability-distribution-and-parameters-for-real-data-python-3
https://stackoverflow.com/questions/6620471/fitting-empirical-distribution-to-theoretical-ones-with-scipy-python?lq=1

## For Part 4
https://stackoverflow.com/questions/33359740/random-number-between-0-and-1-in-python
