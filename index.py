from src.part_1 import *
from src.part_2 import *
from src.part_3 import *
from src.part_4 import *
from src.classes.course import Course

# init our global objects
courses = {
        "FIRST_COURSE": Course("starter"),
        "SECOND_COURSE": Course("main"),
        "THIRD_COURSE": Course("dessert"),
    }
# Part 1
init()
df = create_data()
plot_data(df)
df = compute_drinks(df, courses)

x = input("Part 1 over. \n\n"
      "Press \"n\" to stop the app or anything else to continue : ")
if x == "n":
    exit()

# Part 2
df = create_cluster(df)

# Part 3
df_part3 = create_data_part3()
df = compare_labels(df_part3,df)
pb_client = distribution_clients(df)
pb_course = likelihood_courses(df)
pb_dish = proba_dish(df,courses)
distribution_drinks(df)

# Part 4
# We created class Client
init_simulation(pb_client, pb_course, pb_dish, courses, df)