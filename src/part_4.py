import os

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import random
import pandas as pd
from .classes.client import Client


def init_simulation(proba_client, proba_course, proba_dish, courses, df_init):
    # Prepare the DF
    df = pd.DataFrame(columns=["TIME", "CUSTOMERID", "CUSTOMERTYPE", "COURSE1", "COURSE2", "COURSE3", "DRINKS1",
                               "DRINKS2", "DRINKS3", "TOTAL1", "TOTAL2", "TOTAL3"])
    df_time = []
    df_id = []
    df_type = []
    df_course1 = []
    df_course2 = []
    df_course3 = []
    df_drinks1 = []
    df_drinks2 = []
    df_drinks3 = []
    df_total1 = []
    df_total2 = []
    df_total3 = []

    # Create the slices
    slice_1 = proba_client["Business"]
    slice_2 = slice_1 + proba_client["Healthy"]
    slice_3 = slice_2 + proba_client["Retirement"]
    slice_4 = slice_3 + proba_client["Onetime"]

    drinks1_min = df_init[df_init['FIRST_COURSE_DRINKS'] != 0]['FIRST_COURSE_DRINKS'].min()
    drinks1_max = df_init[df_init['FIRST_COURSE_DRINKS'] != 0]['FIRST_COURSE_DRINKS'].max()
    drinks2_min = df_init[df_init['SECOND_COURSE_DRINKS'] != 0]['SECOND_COURSE_DRINKS'].min()
    drinks2_max = df_init[df_init['SECOND_COURSE_DRINKS'] != 0]['SECOND_COURSE_DRINKS'].max()
    drinks3_min = df_init[df_init['THIRD_COURSE_DRINKS'] != 0]['THIRD_COURSE_DRINKS'].min()
    drinks3_max = df_init[df_init['THIRD_COURSE_DRINKS'] != 0]['THIRD_COURSE_DRINKS'].max()

    for i in range(0, 20 * 365 * 5):
        # We set up a random number that follow the uniform laws in order to create our data.
        x = random.uniform(0, 1)
        # Could have used dichotomy, but as we do not have a lot of values it is ok.
        if x <= slice_1:
            type_client = "Business"
        elif slice_1 <= x <= slice_2:
            type_client = "Healthy"
        elif slice_2 <= x <= slice_3:
            type_client = "Retirement"
        elif slice_3 <= x <= slice_4:
            type_client = "Onetime"
        else:
            print("Critical error for simulation. Close the app.")
            exit()

        # We create the client and its courses and dishes according to the results from part 3.
        # Missing time, couldn't handle the generation of IDs
        client = Client("ID###", type_client)
        client.go_to_restaurant(proba_course, proba_dish)

        # Save data in a list in order to add it into the DF
        df_time.append(client.time)
        df_id.append(client.id)
        df_type.append(client.type)
        df_course1.append(client.first_course)
        df_course2.append(client.second_course)
        df_course3.append(client.third_course)

        # Add drinks data depending of intervals found in part 3.
        # Commenting those lines as we didn't have the time to find a more faster way to add drinks
        if client.first_course != "":
            df_drinks1.append(random.uniform(drinks1_min, drinks1_max))
        else:
            df_drinks1.append(0)

        if client.second_course != "":
            df_drinks2.append(random.uniform(drinks2_min, drinks2_max))
        else:
            df_drinks2.append(0)

        if client.third_course != "":
            df_drinks3.append(random.uniform(drinks3_min, drinks3_max))
        else:
            df_drinks3.append(0)

        df_total1.append(0)
        df_total2.append(0)
        df_total3.append(0)

    # Add the data into the final DF
    df["TIME"] = df_time
    df["CUSTOMERID"] = df_id
    df["CUSTOMERTYPE"] = df_type
    df['COURSE1'] = df_course1
    df['COURSE2'] = df_course2
    df['COURSE3'] = df_course3
    df["DRINKS1"] = df_drinks1
    df["DRINKS2"] = df_drinks2
    df["DRINKS3"] = df_drinks3
    df["TOTAL1"] = df['COURSE1'].map(courses['FIRST_COURSE'].menu) + df["DRINKS1"]
    df["TOTAL2"] = df['COURSE2'].map(courses['SECOND_COURSE'].menu) + df["DRINKS2"]
    df["TOTAL3"] = df['COURSE3'].map(courses['THIRD_COURSE'].menu) + df["DRINKS3"]

    # pd.options.display.max_rows = None
    print(df.shape)
    print(df.dtypes)
    print(df.nunique())
    print(df.describe())
    print(df.head(100))
