import os
import pandas as pd
import matplotlib.pyplot as plt
from .classes.course import Course

currentPath = os.path.dirname(__file__)


# Read and plot data of part1.csv

def init():
    print("Initialization of the Restaurant simulation...")
    # To use if you want to print data or implement menu selection.


def create_data():
    data = os.path.join(currentPath, "../Data/part1.csv")
    return pd.read_csv(data)


def plot_data(df):
    print(df.shape)
    print(df.dtypes)
    print(df.nunique())
    print(df.describe())
    print(df.head())
    labels = ['First Course', 'Second Course', 'Third Course']
    plt.hist(
        [
            df['FIRST_COURSE'],
            df['SECOND_COURSE'],
            df['THIRD_COURSE'],
        ],
        bins=10,
        label=labels,
        ec="k"
    )
    plt.title('Distribution of cost per course', fontsize=20)
    plt.ylabel('Quantity', fontsize=14)
    plt.xlabel('Cost', fontsize=14)
    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)
    plt.legend(prop={'size': 10})
    plt.show()

def compute_drinks(df, courses):
    # Create container to store the data we want to compare
    container = {
        "FIRST_COURSE": [],
        "SECOND_COURSE": [],
        "THIRD_COURSE": []
    }

    # Take the key and value (here class)
    for key, course in courses.items():
        # We get the items of each menu sorted by the most expensive to cheapest.
        # We know that our data will have an order.
        for item in course.get_sorted_menu():
            # We create a DF with the following condition : price should be higher than price of the food item.
            tmp_data = pd.DataFrame(df[df[key] >= course.get_price_of_item(item)][key] - course.get_price_of_item(item))
            tmp_data[key + "_DISH"] = item
            tmp_data.rename(columns={key: key + "_DRINKS"}, inplace=True)
            # We store our results
            container[key].append(tmp_data)

    # We have DFs stored, but our DFs contain "duplicates" : DF[0] is in DF[1] which is in DF[2]...
    # We want to remove duplicates.
    for key, data in container.items():
        # We start with the DF that contains the more duplicates, and "go up" to DF[0] being the one without duplicates.
        for i in reversed(range(1, len(data))):
            data[i] = data[i].drop(data[i - 1].index.values.tolist())

    # We removed duplicates, we can now concat the DFs in order to merge them into our principal DF.
    for key in container.keys():
        df =  df.join(pd.concat(container[key]))

    df = df.fillna(0)
    # Check data
    pd.options.display.max_columns = None
    print(df)
    return df
