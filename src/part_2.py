import os
from sklearn.cluster import KMeans
import seaborn as sns

import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

currentPath = os.path.dirname(__file__)


def create_cluster(df):
    df_data = df[['FIRST_COURSE', "SECOND_COURSE", "THIRD_COURSE"]]
    kmeans = KMeans(n_clusters=4, init='k-means++', random_state=0).fit(df_data)
    # We use seaborn for better plot rendering
    # sns.set(style="darkgrid")
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    # Use matplot color class to select a color panel in order to display different color for clusters.
    cmap = ListedColormap(sns.color_palette("hls", 8).as_hex())

    x = df_data['FIRST_COURSE']
    y = df_data['SECOND_COURSE']
    z = df_data['THIRD_COURSE']

    ax.set_xlabel("Starter")
    ax.set_ylabel("Main")
    ax.set_zlabel("Dessert")

    graph = ax.scatter(x, y, z, s=100, c=kmeans.labels_, marker='o', cmap=cmap)
    plt.legend(*graph.legend_elements(), bbox_to_anchor=(1.05, 1), loc=2)

    centers = kmeans.cluster_centers_
    ax.scatter(centers[:, 0], centers[:, 1], centers[:, 2], marker="X", c="black", s=500, label="centroids")

    plt.show()

    # Answers to questions
    print("\n"
          "\n"
          "##### ANSWERS TO QUESTIONS #####\n"
          "Business client : order the most expensice dishes\n"
          "Healthy client : order healthy food and no dessert\n"
          "Retired client : order 3 courses menu often ending with piece of pie\n"
          "One time client : others\n\n")
    print("Business clients ==> clusters red, they take the most expensive dishes,\n"
          "Healthy clients ==> clusters green, most of them go for soup and salad,\n"
          "Retired clients ==> clusters blue, very often take 3 courses menu, \n"
          "One time clients ==> clusters pink.\n\n")
    print("We noticed that the Healthy Group very ofter take a dessert finally.\n"
          "The One Time Group is not very interested about the starter and usually don't take one.\n\n")

    # Save label and return the principal DF for next part.
    df['K_LABELS'] = kmeans.labels_
    return df