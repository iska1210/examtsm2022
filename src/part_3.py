import os
import random

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats

currentPath = os.path.dirname(__file__)


def create_data_part3():
    data = os.path.join(currentPath, "../Data/part3.csv")
    df_part3 = pd.read_csv(data)
    print("Data of the part3.csv :\n")
    print(df_part3.shape)
    print(df_part3.dtypes)
    print(df_part3.nunique())
    print(df_part3.describe())
    print(df_part3)
    print("\n\n")
    return df_part3


def compare_labels(df_part3, df):
    # Rename our labels in order to do a comparison by string values.
    labels = {
        0: "Business",
        1: "Healthy",
        2: "Retirement",
        3: "Onetime"
    }
    df['K_LABELS'] = df['K_LABELS'].map(labels)
    # We compare labels, if a line match (TRUE) it will be displayed in the DF, thus we can compare with the shapes.
    error = df.shape[0] - df_part3[df_part3['CLIENT_TYPE'] == df['K_LABELS']].shape[0]
    if error > 0:
        print(f"Numbers of errors (number of item different from \"part1.csv\") : {error}\n"
              f"% of error(s) : {round(error / df.shape[0] * 100, 2)}%")
    else:
        print("No error, the accuracy is perfect.")

    # Return the DF with renamed labels
    df = df.rename(columns={'K_LABELS': 'CLIENT_TYPE'})
    return df


def distribution_clients(df):
    # Can you determine the distribution of clients (how many of each group do I expect)
    print("\n"
          "Print the distribution of clients type")
    # Use normalize to get relative frequency
    data = round(df['CLIENT_TYPE'].value_counts(normalize=True), 4)
    print(data * 100, '\n')
    sns.displot(df["CLIENT_TYPE"], stat="probability", bins=df['CLIENT_TYPE'].value_counts().shape[0])
    plt.show()
    return data


def likelihood_courses(df):
    # Can you determine the likelihood for each of these clients to get a certain course.
    # So answering the question “How likely is this type of client to get a starter, main and dessert?”
    print("How likely is this type of client to get a starter, main and dessert?\n")

    data = {}
    for client_type in ["Business", "Healthy", "Retirement", "Onetime"]:
        data_course = {}

        for course in ["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE"]:
            nb_clients_course = df[(df[course] > 0) & (df["CLIENT_TYPE"] == client_type)].shape[0]
            nb_clients_same_type = df[df["CLIENT_TYPE"] == client_type].shape[0]
            likelihood = round(nb_clients_course / nb_clients_same_type, 2)
            data_course[course] = likelihood

            print(f"Clients of type {client_type} are likely to order a {course} {likelihood * 100}% of the time.")

        data[client_type] = data_course
        print('\n')
    return data


def proba_dish(df, courses):
    # Can you figure out the probability of a certain type of customer ordering a certain dish. Here we compute the
    # probability of a client type to get a specific dish given that he ordered the corresponding course. i.e. We do
    # not count clients that do not order course.

    # data will store all the results and return them. It will contain client type as a key and dict as a value.
    data = {}
    for client_type in ["Business", "Healthy", "Retirement", "Onetime"]:
        # Second level of the dict data, it will contain course as a key, and a dict as a value.
        data_course = {}
        print(f"Client Type : {client_type}")
        for course in ["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE"]:
            # Last level of the dict data, it will contains dish as a key and the probability as a value.
            data_dish = {}
            print(f"Course : {course}")
            for dish in courses[course].get_sorted_menu().keys():
                print(f"Dish : {dish}")
                nb_clients_dish = df[(df["CLIENT_TYPE"] == client_type) & (df[course + "_DISH"] == dish)].shape[0]
                nb_clients_same_type_with_course = df[(df[course] > 0) & (df["CLIENT_TYPE"] == client_type)].shape[0]
                proba = round(nb_clients_dish / nb_clients_same_type_with_course, 2)
                data_dish[dish] = proba
                print(
                    f"The probability of a client of type {client_type} during {course} to order {dish} is {proba * 100}%\n")
            data_course[course] = data_dish
        data[client_type] = data_course
        print("\n")
    return data


def distribution_drinks(df):
    # Tried to prove that the distribution is uniform from 0 to 5 (0 excluded)
    # But couldn't get the correct p-value
    # print(stats.kstest(random.uniform(0, 100, size=(1000, 1, 1)), 'uniform'))

    # According to the plots, we will assume that the values are uniform.
    sns.displot(df[df["FIRST_COURSE_DRINKS"] != 0]["FIRST_COURSE_DRINKS"], bins=200)
    sns.displot(df[df["SECOND_COURSE_DRINKS"] != 0]["SECOND_COURSE_DRINKS"], bins=200)
    sns.displot(df[df["THIRD_COURSE_DRINKS"] != 0]["THIRD_COURSE_DRINKS"], bins=200)

    # As a client do not take drinks alone, We take care to not take into account the client that didn't order the
    # corresponding course for our computations about drinks distribution
    no_drink1 = round(df[(df["FIRST_COURSE_DRINKS"] == 0) & (df["FIRST_COURSE"] > 0)]["FIRST_COURSE_DRINKS"].shape[0] / df[df["FIRST_COURSE"]>0]["FIRST_COURSE_DRINKS"].shape[0],4)
    no_drink2 = round(df[(df["SECOND_COURSE_DRINKS"] == 0) & (df["SECOND_COURSE"] > 0)]["SECOND_COURSE_DRINKS"].shape[0] /
                      df[df["SECOND_COURSE"] > 0]["SECOND_COURSE_DRINKS"].shape[0], 4)
    no_drink3 = round(df[(df["THIRD_COURSE_DRINKS"] == 0) & (df["THIRD_COURSE"] > 0)]["THIRD_COURSE_DRINKS"].shape[0] /
          df[df["THIRD_COURSE"] > 0]["THIRD_COURSE_DRINKS"].shape[0], 4)

    print(f"We have:\n"
          f"{no_drink1 * 100}% of the clients do not take drinks for their Starter.\n"
          f"{no_drink2 * 100}% of the clients do not take drinks for their Main.\n"
          f"{no_drink3 * 100}% of the clients do not take drinks for the Dessert.\n")
    print("Thus, we can say that a client always takes drinks when he orders the concerned course.\n")
    print(f"For the rest of the exam we consider the drink distribution uniform over the following intervals :\n")
    print(
        f"- Interval for First Drinks : {df[df['FIRST_COURSE_DRINKS'] != 0]['FIRST_COURSE_DRINKS'].min()}, {df[df['FIRST_COURSE_DRINKS'] != 0]['FIRST_COURSE_DRINKS'].max()}")
    print(
        f"- Interval for Second Drinks : {df[df['SECOND_COURSE_DRINKS'] != 0]['SECOND_COURSE_DRINKS'].min()}, {df[df['SECOND_COURSE_DRINKS'] != 0]['SECOND_COURSE_DRINKS'].max()}")
    print(
        f"- Interval for Third Drinks : {df[df['THIRD_COURSE_DRINKS'] != 0]['THIRD_COURSE_DRINKS'].min()}, {df[df['THIRD_COURSE_DRINKS'] != 0]['THIRD_COURSE_DRINKS'].max()}")

    plt.show()