# Client Class
# As we have some assumptions on client we can create an object.
# Type are hardcoded, no scalability as it is out of scope.
import random


class Client:
    available_types = ['BUSINESS', 'HEALTHY', 'RETIREMENT', 'ONETIME']

    def __init__(self, id: str, type: str):
        # some security checks
        try:
            type.capitalize() in self.available_types
        except ValueError:
            print("Looks like the type of client is not 'BUSINESS', 'SPORT', 'RETIREE' or 'OTHER'"
                  "This version only takes into account those types."
                  "Killing app to avoid bug...")
            exit()

        self.id = id
        self.time = ""
        self.first_course = ""
        self.second_course = ""
        self.third_course = ""
        self.type = type

    # Methods We take in consideration the probability to get a course specific, and if so, the probability to get a
    # specific dish.
    def go_to_restaurant(self, proba_course, proba_dish):

        # We set up here the variable TIME = DINNER | LUNCH
        # We assume a probability of 50% for each value.

        if random.uniform(0, 1) <= 0.5:
            self.time = "LUNCH"
        else:
            self.time = "DINNER"

        for client_type in self.available_types:
            client_type = client_type .capitalize()
            if client_type == self.type:
                for k, v in proba_course[client_type].items():
                    if random.uniform(0, 1) <= v:
                        if k == "FIRST_COURSE":
                            tmp = 0
                            x = random.uniform(0, 1)
                            for key, value in proba_dish[client_type][k].items():
                                if tmp <= x <= (tmp + value):
                                    self.first_course = key
                                tmp = tmp + value
                        elif k == "SECOND_COURSE":
                            tmp = 0
                            x = random.uniform(0, 1)
                            for key, value in proba_dish[client_type][k].items():
                                if tmp <= x <= (tmp + value):
                                    self.second_course = key
                                tmp = tmp + value
                        elif k == "THIRD_COURSE":
                            tmp = 0
                            x = random.uniform(0, 1)
                            for key, value in proba_dish[client_type][k].items():
                                if tmp <= x <= (tmp + value):
                                    self.third_course = key
                                tmp = tmp + value
                        else:
                            print('Critical error. Closing the app')
                            exit()
