# Course Class
# Hardcoded class that contain courses info according to the problem set.
# No scalability as it is out of scope.

class Course:
    available_types = ['STARTER', 'MAIN', 'DESSERT']

    def __init__(self, type: str):
        # some security checks

        try:
            if type.upper() not in self.available_types  :
                raise ValueError

        except ValueError as e:
            print(f"Looks like the type of course '{type}' is not 'STARTER', 'MAIN' or 'DESSERT'\n"
                  "This version only takes into account those types.\n"
                  "Closing app to avoid bug..."
                  "Please restart with correct value.")
            exit()

        else:
            self.type = type.upper()

            if self.type == "STARTER":
                self.menu = {
                    "soup": 3,
                    "tomato-mozzarella": 15,
                    "oysters": 20,
                }
            elif self.type == "MAIN":
                self.menu = {
                    "salad": 9,
                    "spaghetti": 20,
                    "steak": 25,
                    "lobster": 40,
                }
            elif self.type == "DESSERT":
                self.menu = {
                    "ice-cream": 15,
                    "pie": 10,
                }
            else:
                print("else cond")

            print(f"Object created with type : {self.type}"
                  f"\n"
                  f"and items {self.menu}")

    # Gets
    # Sort the menu from most expensive to less
    # Return the whole dict (key and value)
    def get_sorted_menu(self):
        return {k: v for k, v in sorted(self.menu.items(), key=lambda item: item[1], reverse=True)}

    # Return price only (value of the dict)
    def get_price_of_item(self, item):
        try:
            if item not in self.menu:
                raise ValueError
        except ValueError:
            print("This item is not in this course type.")

        return self.menu[item]
